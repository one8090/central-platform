package com.central.cms.service;

import com.central.cms.commons.base.service.BaseService;
import com.central.cms.mybatis.model.CpCmsFriendlinkGroup;

public interface CpCmsFriendlinkGroupService extends BaseService<CpCmsFriendlinkGroup>{

}
