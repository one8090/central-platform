package com.central.cms.service.impl;

import com.central.cms.service.CpCmsAdminService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsAdmin;
import com.central.cms.mybatis.mapper.CpCmsAdminMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsAdminServiceImpl extends BaseServiceImpl<CpCmsAdminMapper, CpCmsAdmin> implements CpCmsAdminService {


}
