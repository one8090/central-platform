package com.central.cms.service.impl;

import com.central.cms.service.CpCmsAdSilderService;
import com.central.cms.commons.base.service.impl.BaseServiceImpl;
import com.central.cms.mybatis.model.CpCmsAdSilder;
import com.central.cms.mybatis.mapper.CpCmsAdSilderMapper;
import org.springframework.stereotype.Service;

@Service
public class CpCmsAdSilderServiceImpl extends BaseServiceImpl<CpCmsAdSilderMapper, CpCmsAdSilder> implements CpCmsAdSilderService {


}
