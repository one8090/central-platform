package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsSearchWords;
import com.central.cms.service.CpCmsSearchWordsService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsSearchWordsController extends BaseController {

    @Autowired
    private CpCmsSearchWordsService cpcmssearchwordsService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsSearchWords cpcmssearchwords = cpcmssearchwordsService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmssearchwords);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmssearchwordsService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsSearchWords cpcmssearchwords){
        cpcmssearchwordsService.insertSelective(cpcmssearchwords);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsSearchWords cpcmssearchwords){
        cpcmssearchwordsService.updateSelectiveById(cpcmssearchwords);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsSearchWords queryBean){
        PageInfo<CpCmsSearchWords> page = cpcmssearchwordsService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
