package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsMember;
import com.central.cms.service.CpCmsMemberService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsMemberController extends BaseController {

    @Autowired
    private CpCmsMemberService cpcmsmemberService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsMember cpcmsmember = cpcmsmemberService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsmember);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsmemberService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsMember cpcmsmember){
        cpcmsmemberService.insertSelective(cpcmsmember);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsMember cpcmsmember){
        cpcmsmemberService.updateSelectiveById(cpcmsmember);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsMember queryBean){
        PageInfo<CpCmsMember> page = cpcmsmemberService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
