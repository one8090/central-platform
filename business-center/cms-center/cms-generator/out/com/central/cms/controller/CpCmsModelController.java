package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsModel;
import com.central.cms.service.CpCmsModelService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsModelController extends BaseController {

    @Autowired
    private CpCmsModelService cpcmsmodelService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsModel cpcmsmodel = cpcmsmodelService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsmodel);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsmodelService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsModel cpcmsmodel){
        cpcmsmodelService.insertSelective(cpcmsmodel);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsModel cpcmsmodel){
        cpcmsmodelService.updateSelectiveById(cpcmsmodel);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsModel queryBean){
        PageInfo<CpCmsModel> page = cpcmsmodelService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
