package com.central.cms.controller;

import com.central.cms.mybatis.model.CpCmsAttachment;
import com.central.cms.service.CpCmsAttachmentService;
import com.github.pagehelper.PageInfo;
import com.central.cms.commons.base.controller.BaseController;
import com.central.cms.commons.base.controller.response.Result;
import com.central.cms.commons.base.controller.response.ResultGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/")
public class CpCmsAttachmentController extends BaseController {

    @Autowired
    private CpCmsAttachmentService cpcmsattachmentService;


    @GetMapping("find/{id}")
    public Result find(@PathVariable("id") Object Id){
        CpCmsAttachment cpcmsattachment = cpcmsattachmentService.findById(Id);
        return ResultGenerator.genSuccessResult().setData(cpcmsattachment);
    }

    @PostMapping("delete/{id}")
    public Result save(@PathVariable("id") Object Id){
        cpcmsattachmentService.deleteById(Id);
        return ResultGenerator.genSuccessResult().setInfo("删除成功！");

    }

    @PostMapping("save")
    public Result save(CpCmsAttachment cpcmsattachment){
        cpcmsattachmentService.insertSelective(cpcmsattachment);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @PostMapping("update")
    public Result update(CpCmsAttachment cpcmsattachment){
        cpcmsattachmentService.updateSelectiveById(cpcmsattachment);
        return ResultGenerator.genSuccessResult().setInfo("保存成功！");

    }

    @GetMapping("page")
    public Result page(@RequestParam(value = "pageNumber",defaultValue = "1") Integer pageNumber,
                        @RequestParam(value = "pageSize",defaultValue = "30") Integer pageSize,
                        CpCmsAttachment queryBean){
        PageInfo<CpCmsAttachment> page = cpcmsattachmentService.page(pageNumber,pageSize,queryBean);
        return ResultGenerator.genSuccessResult().setData(page.getList());

    }




}
